import { Table, Text } from "@mantine/core";

const Team = [
    {
        id: Math.random().toString(),
        fullName: 'Абылайхан А.А.',
        inviteCount: 3,
        profit: 1000,
    },
    {
        id: Math.random().toString(),
        fullName: 'Абылайхан А.А.',
        inviteCount: 2,
        profit: 1000,
    },
    {
        id: Math.random().toString(),
        fullName: 'Абылайхан А.А.',
        inviteCount: 10,
        profit: 1000,
    },
]

const TeamItemTable = () => {


    const rows = Team.map((item) => {
        return (
            <Table.Tr key={item.id}>
                <Table.Td>
                    <Text>{item.fullName}</Text>
                </Table.Td>
                <Table.Td>
                    <Text>{item.inviteCount}</Text>
                </Table.Td>
                <Table.Td>
                    <Text>{item.profit}</Text>
                </Table.Td>
                <Table.Td>
                    <Text>{item.profit}</Text>
                </Table.Td>

            </Table.Tr>
        )
    })

    return (
        <Table.ScrollContainer minWidth={500}>
            <Table>
                <Table.Thead>
                    <Table.Tr>
                        <Table.Th>Данные</Table.Th>
                        <Table.Th>Реферал</Table.Th>
                        <Table.Th>Сумма</Table.Th>
                        <Table.Th>Процент</Table.Th>
                    </Table.Tr>
                </Table.Thead>
                <Table.Tbody>
                    {rows}
                </Table.Tbody>
            </Table>
        </Table.ScrollContainer>

    )
}

export default TeamItemTable;