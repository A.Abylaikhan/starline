import { ActionIcon, Badge, Button, Card, Container, Divider, Flex, Stack, Text } from "@mantine/core";
import { IconCoins, IconEdit } from "@tabler/icons-react";

const UserInfo = () => {
    return (
        <Container fluid px={0}>
            <Stack>
                <Card shadow="xs" padding="xl" bg={'blue'} radius={'md'}>
                    <Text c={'yellow.0'} ta={'center'} size="xl">A</Text>
                </Card>
                <Flex justify={'space-between'} align={'center'}>
                    <Text fw={700} size="xl">Роберт</Text>
                    <Badge color="orange.6">Бронза</Badge>
                </Flex>
                <Flex justify={'space-between'} align={'center'}>
                    <Text fw={700}>Мой кошелек</Text>
                    <Flex align={'center'}>
                        <IconCoins size={20} />
                        <Text fw={700} ml={5} size="xl">10'000 ₸</Text>
                    </Flex>
                </Flex>
                <Divider />
                <Flex justify={'space-between'} align={'center'}>
                    <Text fw={700}>Контакты</Text>
                    <ActionIcon variant="subtle" color="dark" size={'sm'}>
                        <IconEdit />
                    </ActionIcon>
                </Flex>
                <Flex justify={'space-between'}>
                    <Text>example@mail.com</Text>
                    <Text>+7 777 777 77 77</Text>
                </Flex>
                <Divider />
                <Flex justify={'space-between'} align={'center'}>
                    <Text fw={700}>Адрес доставки</Text>
                    <ActionIcon variant="subtle" color="dark" size={'sm'}>
                        <IconEdit />
                    </ActionIcon>
                </Flex>
                <Flex justify={'space-between'} align={'center'}>
                    <Text>Город Алматы</Text>
                    <Text>ул. Абая 123</Text>
                </Flex>
                <Button radius={'md'}>Продлить подписку</Button>
            </Stack>
        </Container>
    );
};

export default UserInfo;