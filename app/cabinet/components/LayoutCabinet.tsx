'use client';

import { Grid } from "@mantine/core";
import UserInfo from "./UserInfo";
import UserMain from "./UserMain";

const LayoutCabinet = () => {
    return (
        <Grid>
            <Grid.Col mih={400} span={{ lg: 4, md: 6, xs: 12 }}>
                <UserInfo />
            </Grid.Col>
            <Grid.Col mih={400} span={{ lg: 8, md: 6, xs: 12 }}>
                <UserMain />
            </Grid.Col>
        </Grid>
    )
}

export default LayoutCabinet;