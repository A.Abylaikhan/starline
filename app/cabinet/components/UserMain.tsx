import { Accordion, AccordionControl, AccordionItem, Button, Container, CopyButton, Divider, Stack, Text } from "@mantine/core";
import { IconLink } from "@tabler/icons-react";
import WalletHistoryItem, { IWalletHistoryItem } from "./WalletHistoryItem";
import OrderHistory, { IProduct } from "./OrdersHistory";
import TeamTable from "./Team";


const walletHistory: IWalletHistoryItem[] = [
    {
        title: 'Оплата заказа',
        value: '1000',
        isNegative: true,
        date: '12.12.2021'
    },
    {
        title: 'Оплата заказа',
        value: '1000',
        isNegative: true,
        date: '12.12.2021'
    },
    {
        title: 'Поступление средств',
        value: '1000',
        isNegative: false,
        date: '12.12.2021'
    },
    {
        title: 'Оплата заказа',
        value: '1000',
        isNegative: true,
        date: '12.12.2021'
    },
    {
        title: 'Оплата заказа',
        value: '1000',
        isNegative: false,
        date: '12.12.2021'
    },
    {
        title: 'Оплата заказа',
        value: '1000',
        isNegative: true,
        date: '12.12.2021'
    },
]

const ordersHistory: IProduct[] = [
    {
        title: 'Товар 1',
        description: 'Описание товара 1',
        amount: 1,
        price: 1000,
        date: '12.12.2021',
        status: 'Оплачен'
    },
    {
        title: 'Товар 2',
        description: 'Описание товара 2',
        amount: 1,
        price: 1000,
        date: '12.12.2021',
        status: 'Оплачен'
    },
    {
        title: 'Товар 3',
        description: 'Описание товара 3',
        amount: 1,
        price: 1000,
        date: '12.12.2021',
        status: 'Оплачен'
    },
    {
        title: 'Товар 4',
        description: 'Описание товара 4',
        amount: 1,
        price: 1000,
        date: '12.12.2021',
        status: 'Оплачен'
    },
    {
        title: 'Товар 5',
        description: 'Описание товара 5',
        amount: 1,
        price: 1000,
        date: '12.12.2021',
        status: 'Оплачен'
    },
];

const UserMain = () => {
    return (
        <Container fluid px={0}>
            <Stack>
                <Text fw={700} size="xl">Реферальная ссылка</Text>
                <CopyButton value="Пример реферальной ссылки">
                    {({ copied, copy }) => (
                        <Button maw={700} size="md" justify="left" leftSection={<IconLink />} color={copied ? 'teal' : 'gray.2'} c={copied ? 'teal.0' : 'dark'} onClick={copy}>
                            {copied ? 'Реферальная ссылка скопирована' : 'Пример реферальной ссылки'}
                        </Button>
                    )}
                </CopyButton>
                <Accordion defaultValue={'team'}>
                    <Accordion.Item value="wallet">
                        <Accordion.Control> <Text fw={700}>История кошелька</Text> </Accordion.Control>
                        <Accordion.Panel>
                            <Stack>
                                {walletHistory.map((item, index) => (
                                    <div key={index}>
                                        <WalletHistoryItem item={item} />
                                        <Divider my={15} />
                                    </div>

                                ))}
                            </Stack>
                        </Accordion.Panel>
                    </Accordion.Item>
                    <Accordion.Item value="orders" px={0}>
                        <Accordion.Control><Text fw={700}>История заказов</Text> </Accordion.Control>
                        <Accordion.Panel px={0}>
                            <OrderHistory products={ordersHistory} />
                        </Accordion.Panel>
                    </Accordion.Item>
                    <Accordion.Item value="team">
                        <Accordion.Control> <Text fw={700}>Моя команда</Text></Accordion.Control>
                        <Accordion.Panel>
                            <Stack>
                                <TeamTable />
                            </Stack>
                        </Accordion.Panel>
                    </Accordion.Item>
                </Accordion>
            </Stack>
        </Container >
    );
}

export default UserMain;