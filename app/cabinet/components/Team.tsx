'use client'

import { Accordion, Flex, Text } from "@mantine/core";
import { IconLock } from "@tabler/icons-react";
import TeamItemTable from "./TeamItemTable";

const Lines = [
    {
        id: Math.random().toString(),
        title: 'Линия 1',
        isActive: true,
    },
    {
        id: Math.random().toString(),
        title: 'Линия 2',
        isActive: true,
    },
    {
        id: Math.random().toString(),
        title: 'Линия 3',
        isActive: false,
    },
    {
        id: Math.random().toString(),
        title: 'Линия 4',
        isActive: false,
    },
    {
        id: Math.random().toString(),
        title: 'Линия 5',
        isActive: false,
    },
]


const TeamTable = () => {
    return (
        <Accordion defaultValue={Lines[0].id}>
            {Lines.map((line) => (
                <Accordion.Item value={line.id} px={0}>
                    <Accordion.Control disabled={!line.isActive}>
                        <Flex align={'center'} gap={10}>
                            <Text fw={600}>
                                {line.title}
                            </Text>
                            {!line.isActive && <IconLock />}
                        </Flex>
                    </Accordion.Control>
                    <Accordion.Panel p={0}>
                        <TeamItemTable />
                    </Accordion.Panel>
                </Accordion.Item>
            ))}
        </Accordion>
    )
}

export default TeamTable;