import { Flex, Text } from "@mantine/core";
import { IconCreditCard } from "@tabler/icons-react";

export interface IWalletHistoryItem {
    title: string;
    value: string;
    isNegative: boolean;
    date: string;
}
const WalletHistoryItem = ({ item }: { item: IWalletHistoryItem }) => {
    return (
        <Flex align={'center'}>
            <IconCreditCard size={30} />
            <Flex direction="column" ml={'md'}>
                <Text fw={700} size={'md'}>{item.title}</Text>
                <Text size="sm">{item.date}</Text>
            </Flex>
            <Flex direction="column" ml={'auto'}>
                {item.isNegative ? <Text fw={700} c="red">-{item.value} ₸</Text> : <Text fw={700} c="green">+{item.value} ₸</Text>}
            </Flex>
        </Flex>
    );
}

export default WalletHistoryItem;