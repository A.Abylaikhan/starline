import { Flex, Table, Text } from "@mantine/core"

export interface IProduct {
    title: string;
    description: string;
    amount: number;
    price: number;
    date: string;
    status: string;
}
interface IOrderHistory {
    products: IProduct[]
}
const OrderHistory = ({ products }: IOrderHistory) => {

    const rows = products.map((product: IProduct) => {
        return (
            <Table.Tr key={product.title}>
                <Table.Td>
                    <Flex direction={'column'}>
                        <Text fw={600}>
                            {product.title}
                        </Text>
                        <Text size="sm">
                            {product.description}
                        </Text>
                    </Flex>
                </Table.Td>
                <Table.Td>
                    <Text fw={600}>
                        {product.amount}
                    </Text>
                </Table.Td>
                <Table.Td>
                    <Text fw={600}>

                        {product.price} ₸
                    </Text>
                </Table.Td>
                <Table.Td>
                    <Text fw={600}>

                        {product.date}
                    </Text>
                </Table.Td>
                <Table.Td>
                    <Text fw={600}>

                        {product.status}
                    </Text>
                </Table.Td>
            </Table.Tr>
        )
    })


    return (
        <Table.ScrollContainer minWidth={500}>
            <Table highlightOnHover px={0}>
                <Table.Thead>
                    <Table.Tr>
                        <Table.Th>Товар</Table.Th>
                        <Table.Th>Количество</Table.Th>
                        <Table.Th>Сумма</Table.Th>
                        <Table.Th>Дата</Table.Th>
                        <Table.Th>Статус</Table.Th>
                    </Table.Tr>
                </Table.Thead>
                <Table.Tbody>
                    {rows}
                </Table.Tbody>
            </Table>
        </Table.ScrollContainer>

    )
}

export default OrderHistory