import { Container, Grid } from "@mantine/core";
import LayoutCabinet from "./components/LayoutCabinet";

const CabinetPage = () => {
    return (
        <Container fluid>
            <LayoutCabinet />
        </Container>
    );
}

export default CabinetPage;