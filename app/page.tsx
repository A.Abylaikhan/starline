import { Container, Text } from "@mantine/core";
import Carousel from "./components/Carousel";
import Offers from "./components/Offers";
import Mission from "./components/Mission";
import About from "./components/About";
import Products from "./components/Products";
import Feedback from "./components/Feedback";

const Home = () => {
  return (
    <Container size={"xl"} px={0}>
      <Carousel />
      <Offers />
      <Mission />
      <About />
      <Products />
      <Feedback />
    </Container>
  );
}

export default Home;