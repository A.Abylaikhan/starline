import { Button, Container, Flex, Stack, Text } from "@mantine/core";
import Image from "next/image";

const Offers = () => {
    return (
        <Container fluid mb={80}>
            <Flex justify={'center'} align={'center'} wrap={'wrap'} gap={20}>
                <Image src={'/gift.png'} alt="Coffee" width={80} height={80} />
                <Stack gap={'2px'} ml={20} ta={'center'}>
                    <Text size="xl" fw={600}>Эксклюзивное предложение!</Text>
                    <Text size="md">Покупайте сейчас и получите бесплатный набор кофе в подарок!</Text>
                </Stack>

                <Button miw={300} radius={"md"}>Заказать</Button>
            </Flex>
        </Container>
    );
}

export default Offers;