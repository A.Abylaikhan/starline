import { Button, Container, Flex, Text } from "@mantine/core";
import ProductItem from "./ProductItem";

const examples = [
    {
        id: Math.random(),
        name: "Starlife 1",
        price: 1000,
        icon: "/example1.png"
    },
    {
        id: Math.random(),
        name: "Starlife 2",
        price: 2000,
        icon: "/example2.png"
    },
    {
        id: Math.random(),
        name: "Starlife 3",
        price: 3000,
        icon: "/example3.png"
    }
];

const Products = () => {
    return (
        <Container fluid p={0} my={80}>
            <Flex justify={'space-between'} mb={40}>
                <Text fw={700} size="xl" component="h1">Продукция</Text>
                <Button variant="subtle" c={'dark.9'}>
                    <Text fw={600}>Смотреть все {">"}</Text>
                </Button>
            </Flex>
            <Flex mt={20} justify={'space-between'} wrap={'wrap'} gap={40}>
                {examples.map((item) => (
                    <ProductItem key={item.id} id={item.id} name={item.name} price={item.price} icon={item.icon} />
                ))}
            </Flex>
        </Container>
    );
}

export default Products;