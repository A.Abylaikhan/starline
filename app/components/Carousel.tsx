'use client';
import { Card, Container, Flex, Text } from "@mantine/core";
import { Carousel } from '@mantine/carousel';
import Image from "next/image";
import Autoplay from 'embla-carousel-autoplay';




const CarouselMain = () => {
    return (
        <Container fluid px={0}>
            <Carousel
                plugins={[Autoplay()]}
                onMouseEnter={Autoplay().pause}
                onMouseLeave={Autoplay().reset}
                withIndicators={true}
                withControls={false}
                loop
                slideGap="md"
                align="start"
                my={50}
            >
                <Carousel.Slide>
                    <Card shadow="xs" padding="xl" bg={'blue'} radius={'md'}>
                        <Flex justify={'space-around'} align={'center'} wrap={'wrap'}>
                            <Flex direction={'column'} c={'blue.0'}>
                                <Text>Насладитесь свежим кофе каждый день! 🌟 </Text>
                                <Text fz={'30px'} maw={400}>Новая кофемашина для идеального вкуса. ☕️</Text>
                            </Flex>
                            <Flex pos={'relative'} mih={280} w={'90%'} maw={400}>
                                <Image src={'/CarouselImage.png'} alt="Coffee" fill />
                            </Flex>
                        </Flex>
                    </Card>
                </Carousel.Slide>
                <Carousel.Slide>
                    <Card shadow="xs" padding="xl" bg={'blue'} radius={'md'}>
                        <Flex justify={'space-around'} align={'center'} wrap={'wrap'}>
                            <Flex direction={'column'} c={'blue.0'}>
                                <Text>Насладитесь свежим кофе каждый день! 🌟 </Text>
                                <Text fz={'30px'} maw={400}>Новая кофемашина для идеального вкуса. ☕️</Text>
                            </Flex>
                            <Flex pos={'relative'} mih={280} w={'90%'} maw={400}>
                                <Image src={'/CarouselImage.png'} alt="Coffee" fill />
                            </Flex>
                        </Flex>
                    </Card>
                </Carousel.Slide>
                <Carousel.Slide>
                    <Card shadow="xs" padding="xl" bg={'blue'} radius={'md'}>
                        <Flex justify={'space-around'} align={'center'} wrap={'wrap'}>
                            <Flex direction={'column'} c={'blue.0'}>
                                <Text>Насладитесь свежим кофе каждый день! 🌟 </Text>
                                <Text fz={'30px'} maw={400}>Новая кофемашина для идеального вкуса. ☕️</Text>
                            </Flex>
                            <Flex pos={'relative'} mih={280} w={'90%'} maw={400}>
                                <Image src={'/CarouselImage.png'} alt="Coffee" fill />
                            </Flex>
                        </Flex>
                    </Card>
                </Carousel.Slide>
            </Carousel>
        </Container>
    );
}

export default CarouselMain;