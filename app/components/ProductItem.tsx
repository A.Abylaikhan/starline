'use client';

import { Button, Flex, Stack, Text } from "@mantine/core";
import Image from "next/image";

interface Product {
    id: number;
    name: string;
    price: number;
    icon: string;
}

const ProductItem = ({ name, price, icon }: Product) => {
    return (
        <Flex gap={20} align={'center'} miw={400} maw={'40%'} justify={'center'}>
            <Image src={icon} alt={name} width={150} height={180} />
            <Stack gap={5} maw={130}>
                <Text size="xl" fw={700}>{name}</Text>
                <Text size="sm"> <Text span fw={600}>Цена:</Text>  {price}</Text>
                <Text size="sm"> <Text span fw={600}>Тип:</Text> Капсульный</Text>
                <Text size="sm"> <Text span fw={600}>Описание:</Text> Lorem ipsum dolor sit amet.</Text>
                <Button mt={'auto'} miw={130}>Купить</Button>
            </Stack>
        </Flex>
    );
};

export default ProductItem;