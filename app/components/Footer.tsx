import { Anchor, Box, Container, Flex, Text } from "@mantine/core";

const Footer = () => {
    return (
        <footer>
            <Container fluid mt={40} py={20} style={{ borderTop: '1px solid #eaeaea' }}>
                <Flex justify={'space-between'} wrap={'wrap'} gap={40}>
                    <Text fw={700}>© 2024 Star Life. Все права защищены</Text>
                    <Flex gap={30} wrap={'wrap'}>
                        <Anchor href={'#'}>
                            <Text c={'dark'} td='underline' fw={700}>Договор оферты</Text>
                        </Anchor>
                        <Anchor href={'#'}>
                            <Text c={'dark'} td='underline' fw={700}>Политика конфиденциальности</Text>
                        </Anchor>

                    </Flex>
                </Flex>
            </Container>
        </footer>
    );
}

export default Footer;