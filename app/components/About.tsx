'use client';
import { Badge, Container, Flex, List, Stack, Text, ThemeIcon } from "@mantine/core";
import Image from "next/image";
const About = () => {
    return (
        <Container fluid mt={40} p={0}>
            <Text fw={700} size="xl" component="h1">О компании</Text>
            <Flex justify={'space-around'} mt={40} wrap={'wrap-reverse'} gap={20} >
                <Stack maw={600}>
                    <Image src={'/machine1.png'} alt="Delonghi" width={350} height={300} />
                    <Text fw={700} size="lg">Наша география:</Text>
                    <Flex gap={5} wrap={"wrap"} maw={400}>
                        <Badge w={100} color="dark.9">Казахстан</Badge>
                        <Badge w={100} color="dark.9">Кыргызстан</Badge>
                        <Badge w={100} color="dark.9">Узбекистан</Badge>
                        <Badge w={100} color="dark.9">Россия</Badge>
                    </Flex>
                </Stack>
                <Stack maw={500}>
                    <Text fw={700} size="xl">Starlife - это международная компания</Text>
                    <Text size="lg"> <Text span fw={700}>Starlife</Text> – молодая и амбициозная компания с инновационной системой вознаграждения своих партнеров.</Text>

                    <Text fw={700} size="lg" mt={40}>Польза от употребления кофе:</Text>
                    <List spacing={5}>
                        <List.Item icon={
                            <Image src={'/cognitive.svg'} alt="Brain" width={20} height={20} />
                        }>Улучшение когнитивной функции</List.Item>
                        <List.Item icon={
                            <Image src={'/antioxi.svg'} alt="Brain" width={20} height={20} />
                        }>Антиоксиданты</List.Item>
                        <List.Item icon={
                            <Image src={'/decrease.svg'} alt="Brain" width={20} height={20} />
                        }>Снижение риска некоторых заболеваний</List.Item>
                        <List.Item icon={
                            <Image src={'/better.svg'} alt="Brain" width={20} height={20} />
                        }>Улучшение настроения</List.Item>
                        <List.Item icon={
                            <Image src={'/increase.svg'} alt="Brain" width={20} height={20} />
                        }>Повышение физической выносливости</List.Item>
                    </List>
                </Stack>
            </Flex>

        </Container>
    );
}

export default About;