import { Button, Card, Checkbox, Container, Flex, Group, Stack, Text, TextInput } from "@mantine/core";
import Image from "next/image";

const Feedback = () => {
    return (
        <Container fluid p={0} mt={40}>
            <Card bg={'dark.9'} radius={'lg'} c={'dark.0'} px={30}>
                <Flex justify={'space-between'} wrap={'wrap'} gap={40}>
                    <Stack justify="space-between">
                        <Text size="40px">Стать бизнес партнером</Text>
                        <Stack visibleFrom="xs">
                            <Text fw={700}>Следите за нами</Text>
                            <Flex gap={20}>
                                <Image src={'/inst.svg'} alt="Facebook" width={30} height={30} />
                                <Image src={'/tiktok.svg'} alt="Facebook" width={30} height={30} />
                                <Image src={'/youtube.svg'} alt="Facebook" width={30} height={30} />
                            </Flex>
                        </Stack>
                    </Stack>
                    <Stack maw={650}>
                        <Text fw={700}>Заполните все данные которые указаны в форме и наши менеджеры свяжутся с вами</Text>
                        <Group gap={5}>
                            <TextInput miw={300} radius={'md'} placeholder="Имя" />
                            <TextInput miw={300} radius={'md'} placeholder="Телефон" />
                            <TextInput miw={300} radius={'md'} placeholder="Email" />
                            <TextInput miw={300} radius={'md'} placeholder="Город" />
                        </Group>
                        <Checkbox c={'dark.0'} label={'Я соглашаюсь с политикой компании в области персональных данных'} />
                        <Checkbox c={'dark.0'} label={'Я даю согласие на обработку персональных данных'} />
                        <Button radius={'md'} mt={40}>Заказать</Button>
                    </Stack>

                </Flex>
            </Card>
        </Container>
    )
}

export default Feedback;