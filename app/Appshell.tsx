'use client'
import { AppShell, Avatar, Burger, Button, Container, Flex, Group, Select, Stack, Text, UnstyledButton } from '@mantine/core';
import classes from './MobileNavbar.module.css';
import { useDisclosure } from '@mantine/hooks';
import Image from 'next/image';
import Footer from './components/Footer';
import Link from 'next/link';
import { usePathname, useRouter } from 'next/navigation';
import { useEffect } from 'react';

const Appshell = ({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) => {
    const [opened, { toggle }] = useDisclosure();
    const router = useRouter();
    const pathname = usePathname();

    const handleLogoClick = () => {
        router.push('/');
    }
    useEffect(() => {
        if (opened) {
            toggle();
        }
    }, [pathname]);
    return (
        <AppShell
            header={{ height: 60 }}
            navbar={{ width: 300, breakpoint: 'sm', collapsed: { desktop: true, mobile: !opened } }}
            padding="md"
        >
            <AppShell.Header>
                <Group h="100%" px="md">
                    <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />
                    <Group justify="space-between" style={{ flex: 1 }}>
                        <Image style={{ cursor: 'pointer' }} onClick={handleLogoClick} src={'/logo.svg'} alt="Logo" width={100} height={50} />
                        <Group ml="xl" gap={40} visibleFrom="sm">
                            <UnstyledButton className={classes.control}>Интернет магазин</UnstyledButton>
                            <UnstyledButton className={classes.control}>Миссия</UnstyledButton>
                            <UnstyledButton className={classes.control}>Продукт</UnstyledButton>
                            <UnstyledButton className={classes.control}>Партнерство</UnstyledButton>
                            <Flex align={'center'} gap={10}>
                                <Text fw={600}>Страна: </Text>
                                <Select
                                    data={[
                                        { value: 'ru', label: 'Россия' },
                                        { value: 'ua', label: 'Украина' },
                                        { value: 'kz', label: 'Казахстан' },
                                    ]}
                                    defaultValue="kz"
                                    radius="md"
                                    size="sm"
                                />
                            </Flex>
                            <Button component={Link} href={'/cabinet'} radius={'md'} ml={40}>Войти</Button>
                        </Group>
                    </Group>
                </Group>
            </AppShell.Header>

            <AppShell.Navbar py="md" px={4}>
                <Stack pl={20}>
                    <UnstyledButton className={classes.control}>Интернет магазин</UnstyledButton>
                    <UnstyledButton className={classes.control}>Миссия</UnstyledButton>
                    <UnstyledButton className={classes.control}>Продукт</UnstyledButton>
                    <UnstyledButton className={classes.control}>Партнерство</UnstyledButton>
                    <Flex align={'center'} gap={10}>
                        <Text fw={600}>Страна: </Text>
                        <Select
                            data={[
                                { value: 'ru', label: 'Россия' },
                                { value: 'ua', label: 'Украина' },
                                { value: 'kz', label: 'Казахстан' },
                            ]}
                            defaultValue="kz"
                            radius="md"
                            size="sm"
                        />
                    </Flex>
                    <UnstyledButton className={classes.control} component={Link} href={'/cabinet'} maw={100}>Войти</UnstyledButton>
                </Stack>
            </AppShell.Navbar>

            <AppShell.Main>
                {children}
                <Footer />

            </AppShell.Main>
        </AppShell>
    )
}

export default Appshell